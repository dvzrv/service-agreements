DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT
This is revision 1, effective since ISO DATE HERE.

Arch Linux Privacy Policy
=========================

This policy describes how Arch Linux collects, uses, protects and discloses user information
collected by Arch Linux services, and provides information about the choices you have regarding the
ways in which your personal information is manipulated.

The scope of this privacy policy extends to all public as well as internal services operated by
Arch Linux, including in particular all web services provided on the `archlinux.org` domain and its
subdomains.

For convenience, Arch Linux is referred to in this document as "Arch".

_Table of contents_:

[[_TOC_]]

Our commitment to privacy and data security
-------------------------------------------

Arch values your privacy. To better protect your privacy, we have provided this policy explaining
our information practices and the choices you can make about the way your personal information is
collected, used and disclosed.

Arch staff members and service administrators are familiar with our privacy policy guidelines. Our
websites enforce [Transport Layer Security](https://en.wikipedia.org/wiki/Transport_Layer_Security)
(TLS), which encrypts the communication channel you use when you send your personal information to
our websites. Arch is committed to provide its services from secure systems to prevent unauthorized
access to your personal information.

The information we collect
--------------------------

This privacy policy applies to all information collected by or submitted to Arch services,
including personal data. "Personal data" is data that can be used to identify an individual.

Arch collects certain information for statistical and security purposes whenever you access an Arch
service. This includes standard information that web browsers typically make available to the web
servers, notably:

- the pages you visited on our websites,
- the date and time you access a website,
- the name and version of your browser,
- your public IP address.

Arch collects personal data when:

- you create a user account,
- you post comments on the boards, AUR, bug tracker or mailing lists,
- you create content on the wiki,
- you submit packages to the AUR.

Arch may also collect personal data from individuals (with their consent) who participate and/or
contribute to Arch. The types of personal data collected may include (but are not limited to):

- your first and last name,
- your username,
- your country code,
- your e-mail address,
- any information that Arch collects online from you and maintains in association with your account,
  such as:
    - your system password for the forums, wiki, or bug tracker,
    - your GPG key ID,
    - your SSH public key,
    - your IRC nickname,
    - your IP address,
    - your language preference,
    - your timezone,
    - your geographic coordinates (longitude/latitude),
    - your disclosed affiliation(s).

Publicly available personal data
--------------------------------

Some personal data attached to Arch accounts is made public by default _if you opt to include it in
your profile_. Specifically:

- your GPG key ID (if defined);
- your location (if defined)
- your website, blog, or other affiliations (if defined).

If you wish for this information to be kept private, you can opt-out of displaying this information
publicly in your account profile. If you choose to opt-out, Arch will still have access to this
information, but it will not be displayed to others, and will be considered private.

Using (processing) your personal data
-------------------------------------

Arch uses the personal data you provide to:

- create and maintain your accounts;
- identify and authenticate you;
- attribute data and content you produce directly and indirectly in our public-facing services;
- answer your questions;
- send you information;
- for research activities, such as the production of statistical reports (such aggregated
  information is not used to contact the subjects of the report).

Sharing your personal data
--------------------------

Unless you consent, Arch will never process or share the personal data you provide to us except as
described below.

Arch may share your personal data with third parties under any of the following circumstances:

- Your publicly available personal data in the Arch account system, as described above, is
  accessible by anyone unless you, as the account holder, opt out as already described in this
  privacy policy.
- As required by law (such as responding to a valid subpoena, warrant, audit, or agency action,
  or to prevent fraud).
- For research activities, including the production of statistical reports (such aggregated
  information is used to describe our services and is not used to contact the subjects of the
  report).

Receiving e-mail
----------------

Arch may send you e-mail about your account, to communicate with you about your accounts, or in
response to your questions. For your protection, Arch may contact you in the event that we find an
issue that requires your immediate attention. Arch processes your personal data in these cases to
fulfill and comply with its contractual obligations to you, to provide the services you have
requested, and to ensure the security of your account.

Cookies and other browser information
-------------------------------------

Arch's online services automatically capture IP addresses. We use IP addresses to help diagnose
problems with our servers, to administer our websites, and to help ensure the security of your
interaction with our services. Your IP address is used to help identify you and your location, in
order to provide you data and content from our services as quickly as possible. It is in the
interests of the Arch community to maximize the efficiency and effectiveness of its services for
all users.

As part of offering and providing customizable and personalized services, Arch websites use cookies
to store and sometimes track information about you. A cookie is a small amount of data that is sent
to your browser from a web server and stored on your computer's hard drive. All websites provided
by Arch where you are prompted to log in or that are customizable require your browser to accept
cookies.

Generally, we use cookies to:

1. Remind us of who you are and to access your account information (stored on our computers) in
   order to provide a better and more personalized service. This cookie is set when you register or
   "sign in" and is modified when you "sign out" of our services.
2. Estimate audience size. Each browser accessing an Arch website is given a unique cookie that is
   used to determine the extent of recurrent usage and usage by a registered user versus by an
   unregistered user.
3. Measure certain traffic patterns, which areas of Arch's network of websites you have visited,
   and your visiting patterns in the aggregate. We use this research to understand how our
   infrastructure needs to scale to meet demand.

If you do not want your personal information to be stored by cookies, you can configure your
browser so that it always rejects these cookies or asks you each time if you accept them or not.
However, you must understand that the use of cookies may be necessary to provide certain services
(see 1. above), and choosing to reject cookies will reduce the performance and functionality of the
service. Your browser documentation includes instructions explaining how to enable, disable or
delete cookies at the browser level (usually located in the "Help", "Tools" or "Edit" facility).

Public forums reminder
----------------------

Arch makes its public services, including [wiki](https://wiki.archlinux.org/),
[IRC chat rooms](https://wiki.archlinux.org/index.php/Arch_IRC_channels),
[bulletin boards](https://bbs.archlinux.org/), [mailing lists](https://lists.archlinux.org/),
[bug tracker](https://bugs.archlinux.org/), [AUR](https://aur.archlinux.org/) and a
[GitLab instance](https://gitlab.archlinux.org/), available to its users. Please remember that _any
information_ that is disclosed in these areas becomes __public information__. Exercise caution when
deciding to disclose your personal data. Although we value individual ideas and encourage free
expression, Arch reserves the right to take necessary action to preserve the integrity of these
areas, such as removing any posting that is vulgar or inappropriate. See our
[Code of conduct](code-of-conduct.md) for what is acceptable behavior.

It is in the interest of the Arch community to provide all users an accurate record of data and
content provided in the public forums it maintains and uses; to maintain the integrity of that data
and content for historical, scientific, and research purposes; and to provide an environment for
the free exchange of ideas relevant and constructive to the development and propagation of open
source software.

About links to other sites
--------------------------

Arch websites contain links to other sites. Arch does not control the information collection of
sites that can be reached through links from Arch websites. If you have questions about the data
collection procedures of linked sites, please contact those sites directly.

Your rights under GDPR in the EEA
---------------------------------

Where the [EU General Data Protection Regulation](
https://eur-lex.europa.eu/legal-content/EN/TXT/HTML/?uri=CELEX:32016R0679&from=EN)
2016/679 ("GDPR") applies to the processing of your personal data, especially when you access an
Arch service from a country in the European Economic Area ("EEA"), you have the following rights,
subject to some limitations, against Arch:

- The right to access your personal data;
- The right to rectify the personal data we hold about you;
- The right to erase your personal data;
- The right to restrict our use of your personal data;
- The right to object to our use of your personal data;
- The right to receive your personal data in a usable electronic format and transmit it to a third
  party (also known as the right of data portability); and
- The right to lodge a complaint with your local data protection authority.

If you would like to exercise any of these rights, please contact the administrators of the forum,
wiki, or bug tracker where your information is stored. Please understand, however, the rights
enumerated above are not absolute in all cases. Nor does this extend to public information (see the
[Public forums reminder](#public-forums-reminder) section).

Where the GDPR applies, you also have the right to withdraw any consent you have given to uses of
your personal data. If you wish to withdraw consent that you have previously provided to Arch, you
may do so via email to the relevant administrators. However, the withdrawal of consent will not
affect the lawfulness of processing based on consent before its withdrawal.

How to access, modify or update your information
------------------------------------------------

Arch gives you the ability to access, modify or update your personal data at any time. You may log
in and make changes to your login information (change your password), your contact information,
your general preferences and your personalization settings. If necessary, you may also contact us
and describe the changes you want made to the personal data you have previously provided via email.

If you wish to remove your personal data from Arch, you may contact us via email and request that
we remove this information from the relevant Arch account system. Other locations where you may
have used your personal data as an identifier (e.g. forum or bug tracker comments, list postings in
the archives, wiki change history, and spec changelogs) will not be altered.

How to contact us
-----------------

If you have any questions about any of these practices or Arch's use of your personal information,
please feel free to contact us at [privacy@archlinux.org](mailto:privacy@archlinux.org), and we
will work with you to resolve any concerns you may have about this policy.

Changes to this policy
----------------------

Arch reserves the right to change this policy from time to time. If we do make changes, the revised
policy will be posted on this site and a notice will be posted to our
[home page](https://archlinux.org/) whenever this privacy statement is changed in a material
way. Your continued use of Arch services after any change in this policy will constitute your
acceptance of such change.

Credits
-------

This document is based on the
[Fedora Project's Privacy Policy](https://fedoraproject.org/wiki/Legal:PrivacyPolicy), used under
the terms of the [CC BY-SA 3.0](https://creativecommons.org/licenses/by-sa/3.0/) license.
This document is licensed under the terms of
[CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
