DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT
This is revision 1, effective since ISO DATE HERE.

Arch Linux Terms of Service
===========================

The following terms and conditions govern all use of the Arch Linux Services
and all content and services available at or through the
website. Our Services are offered subject to your acceptance without
modification of all of the terms and conditions contained herein and all other
operating rules, policies (including, without limitation, our Privacy Policy)
and procedures that may be published from time to time by Arch Linux. You agree
that we may automatically upgrade our Services, and these terms will apply to
any upgrades.

Please read this Agreement carefully before accessing or using our Services. By
accessing or using any part of our services, you agree to become bound by the
terms and conditions of this agreement. If you do not agree to all the terms
and conditions of this agreement, then you may not access or use any of our
services.

Our Services are not directed to children younger than 13, and access and use
of our Services is only offered to users 13 years of age or older. If you are
under 13 years old, please do not register to use our Services. Any person who
registers as a user or provides their personal information to our Services
represents that they are 13 years of age or older.

Responsibility of Contributors
------------------------------

Some uses of our Services require creating an account. You agree to provide
us with complete and accurate information when you register for an account. You
will be solely responsible and liable for any activity that occurs under your
user name. If you operate an account, maintain a package, upload package
contents, comment on a package or issue or merge request, file a package request,
post material to the Arch Linux User Repository or the Bulletin Board System or
GitLab, post links, or otherwise make (or allow any third
party to make) material available (any such material, Content), you are
entirely responsible for the content of, and any harm resulting from, that
Content or your conduct. That is the case regardless of what form the Content
takes, which includes, but is not limited to text, photo, video, audio, or
code.

You agree not to use the service to upload, post or make available otherwise
any content that is unlawful, harmful, threatening, abusive, harassing, vulgar,
sexually explicit, invasive of another's privacy, hateful, or racially,
ethnically or otherwise objectionable. You agree not to use the service to
upload, post or make available otherwise any content that is copyrighted
without the content author's consent. You agree not to use the service to
upload, post or make available otherwise any material that contains software
viruses or other malicious code, or any illegal copies of software.

By submitting content to any Arch Linux Service, you agree to license the content
under the terms of the GNU General Public License version 3 unless otherwise
stated by the Service. In the case of a Contribution to an Arch Linux Project
via GitLab or otherwise, you agree to license the Contribution under the Project's
license. If the Project doesn't state a license, you agree to license it under the
terms of the GNU General Public License version 3.

If you delete content, you acknowledge that caching or references to the
content may not be made immediately unavailable.

You are responsible for keeping your password and any multi-factor authentication
tokens and devices secure.

You are responsible for ensuring continued access to your account by securely
backing up the password and any multi-factor authentication tokens and devices.
If you lose access to your account, Arch Linux may or may not be able to assist
you in regaining access to your account.

Without limiting any of those representations or warranties, Arch Linux has the
right (though not the obligation) to, in Arch Linux's sole discretion, (i)
reclaim your username or website’s URL due to prolonged inactivity, (ii) refuse
or remove any content that, in our reasonable opinion, violates any Arch Linux
policy or is in any way harmful or objectionable, or (iii) terminate or deny
access to and use of the Services to any individual or entity for any reason.

In addition, you agree to behave according to our [Code of conduct](code-of-conduct.md).

Responsibility of Visitors
--------------------------

Arch Linux has not reviewed, and cannot review, all of the material, including
computer software, posted to our Services, and cannot therefore be responsible
for that material’s content, use or effects. By operating our Services, Arch
Linux does not represent or imply that it endorses the material there posted,
or that it believes such material to be accurate, useful, or non-harmful. You
are responsible for taking precautions as necessary to protect yourself and
your computer systems from viruses, worms, Trojan horses, and other harmful or
destructive content. Our Services may contain content that is offensive,
indecent, or otherwise objectionable, as well as content containing technical
inaccuracies, typographical mistakes, and other errors. Our Services may also
contain material that violates the privacy or publicity rights, or infringes
the intellectual property and other proprietary rights, of third parties, or
the downloading, copying or use of which is subject to additional terms and
conditions, stated or unstated. Arch Linux disclaims any responsibility for any
harm resulting from the use by visitors of our Services, or from any
downloading by those visitors of content there posted.

We have not reviewed, and cannot review, all of the material, including
computer software, made available through the websites and webpages to which
our Services may refer, and that refer to our Services. Arch Linux does not have
any control over those non-Arch Linux websites, and is not responsible for their contents
or their use. By linking to a non-Arch Linux website, Arch Linux does not
represent or imply that it endorses such website. You are responsible for
taking precautions as necessary to protect yourself and your computer systems
from viruses, worms, Trojan horses, and other harmful or destructive content.
Arch Linux disclaims any responsibility for any harm resulting from your use of
non-Arch Linux websites and webpages.

Disclaimer of Warranties
------------------------

Our Services are provided "as is". Arch Linux and its suppliers and licensors
hereby disclaim all warranties of any kind, express or implied, including,
without limitation, the warranties of merchantability, fitness for a particular
purpose and non-infringement. Neither Arch Linux nor its suppliers and
licensors, makes any warranty that our Services will be error free or that
access thereto will be continuous or uninterrupted. You understand that you
download from, or otherwise obtain content or services through, our Services at
your own discretion and risk.

Limitation of Liability
-----------------------

In no event will Arch Linux or the Arch Linux staff be liable with respect to
any subject matter of this Agreement under any contract, negligence, strict
liability or other legal or equitable theory for: (i) any special, incidental
or consequential damages; (ii) the cost of procurement for substitute products
or services; or (iii) for interruption of use or loss or corruption of data.
Arch Linux shall have no liability for any failure or delay due to matters
beyond their reasonable control. The foregoing shall not apply to the extent
prohibited by applicable law.

Termination
-----------

We may terminate your access to all or any part of our Services at any time,
with or without cause, with or without notice, effective immediately. If you
wish to terminate this Agreement or your registered account (if you have one),
you may simply discontinue using our Services. All provisions of this Agreement
which by their nature should survive termination shall survive termination,
including, without limitation, ownership provisions, warranty disclaimers,
indemnity and limitations of liability.

Changes
-------

We are constantly updating our Services, and that means sometimes we have to
change the legal terms under which our Services are offered. If we make changes
that are material, we will let you know by posting on one one of our news pages,
on one of our mailing lists or by sending you an email or other communication before
the changes take effect. The notice will designate a reasonable period of time
after which the new terms will take effect. If you disagree with our changes,
then you should stop using our Services within the designated notice period.
Your continued use of our Services will be subject to the new terms. However,
any dispute that arose before the changes shall be governed by the Terms
(including the binding individual arbitration clause) that were in place when
the dispute arose.

A full list of all changes and date of last change for all of our legal documents is
available here: https://gitlab.archlinux.org/archlinux/service-agreements


About This Document
-------------------

This document was derived from the Automattic Terms of Service and is licensed
under [CC BY-SA 4.0](https://creativecommons.org/licenses/by-sa/4.0/).
