# Service Agreements

Legal agreements concerning Arch Linux services and contributions

- [**Privacy Policy**](privacy-policy.md)
- [**Terms of Service**](terms-of-service.md)
- [**Code of Conduct**](code-of-conduct.md)

## Changing these documents

As per our Terms of Service, if we want to make changes to these legal documents, in order for the changes to be
legally effective, we'll have to announce any changes **at least four weeks** before they become effective.
The announcement will have to be made directly to every registered user via email. 

Generally speaking, we need to try very hard not to make changes willy-nilly as the process is quite arduous. 
If changes need to be made, the process shall be as such:

1. Try to gauge interest in the change and try to find reasons that necessitate it. Such reasons could including a changing legal framework or changing services.
1. Create an MR with the proposed changes.
1. Announce the MR on [arch-dev-public](https://lists.archlinux.org/listinfo/arch-dev-public).
1. Allow a discussion period of 14 days to pass before announcing the result.
1. If the changes are complex and require legal council, we'll engage a lawyer at this point after we're happy with the proposed changes. If the lawyer has some comments, we'll apply them without further discussion.
1. Announce the proposed changes to all users via email and mention the effective date as **today + 1 month**.
1. Merge the MR on the effective date.
